<?php

namespace D3JDigital\Notes;

use OpenSourceDeveloper\Reaktr\Core\Abstracts\Plugin as ReaktrPlugin;
use OpenSourceDeveloper\Reaktr\Core\Contracts\Services\RoutingService;
use D3JDigital\Notes\Contracts\Repositories\NoteRepositoryInterface;
use D3JDigital\Notes\Contracts\Services\NoteServiceInterface;
use D3JDigital\Notes\Database\Eloquent\Repositories\NoteRepository;
use D3JDigital\Notes\Services\NoteService;

class Plugin extends ReaktrPlugin
{
    public ?string $pluginName = 'Notes';
    public ?string $pluginKey = 'notes';
    public ?string $pluginVendorName = 'D3JDigital';
    public ?string $pluginVendorKey = 'd3j-digital';
    public ?string $pluginVersion = 'v0.0.1';
    public ?string $pluginDescription = 'Adds note functionality';
    public ?array $pluginDependencies = [];

    public array $singletons = [
        NoteRepositoryInterface::class => NoteRepository::class,
        NoteServiceInterface::class => NoteService::class,
    ];

    public function registerRoutes(): void
    {
        $routingService = app(RoutingService::class);
        $routingService->registerApiRoutes('D3JDigital\Notes\Request\Controllers\Api', __DIR__ . '/Request/Routes/Api/v1.php', 1);
    }

    public function registerMigrations(): void
    {
        $this->loadMigrationsFrom(__DIR__ . '/Database/Migrations');
    }
}
