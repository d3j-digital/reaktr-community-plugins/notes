<?php

namespace D3JDigital\Notes\Request\Controllers\Api;

use D3JDigital\Notes\Response\Resources\NoteResource;
use Illuminate\Http\JsonResponse;
use OpenSourceDeveloper\Reaktr\Core\Abstracts\Controller;
use OpenSourceDeveloper\Reaktr\Core\Contracts\Services\RoutingService;
use D3JDigital\Notes\Contracts\Services\NoteServiceInterface;
use D3JDigital\Notes\Request\Validation\StoreNote;
use D3JDigital\Notes\Request\Validation\UpdateNote;

class NoteController extends Controller
{
    /**
     * @param RoutingService $routingService
     * @param NoteServiceInterface $noteService
     */
    public function __construct(
        protected RoutingService $routingService,
        protected NoteServiceInterface $noteService,
    ) {
        parent::__construct();
        $this->middleware('UserIsAuthenticated:jwt');
        $this->middleware('UserIsAuthorised:notes.index')->only('index');
        $this->middleware('UserIsAuthorised:notes.show')->only('show');
        $this->middleware('UserIsAuthorised:notes.store')->only('store');
        $this->middleware('UserIsAuthorised:notes.update')->only('update');
        $this->middleware('UserIsAuthorised:notes.destroy')->only('destroy');
    }

    /**
     * @return JsonResponse
     */
    public function index(): JsonResponse
    {
        return $this->routingService->renderJsonSuccessResponse(200, '', NoteResource::collection($this->noteService->getNotes($this->relations, $this->columns)));
    }

    /**
     * @param StoreNote $request
     * @return JsonResponse
     */
    public function store(StoreNote $request): JsonResponse
    {
        return $this->routingService->renderJsonSuccessResponse(201, '', new NoteResource($this->noteService->createNote($request->validated())));
    }

    /**
     * @param int $id
     * @return JsonResponse
     */
    public function show(int $id): JsonResponse
    {
        if ($resource = $this->noteService->getNote($id, $this->relations, $this->columns)) {
            return $this->routingService->renderJsonSuccessResponse(200, '', new NoteResource($resource));
        }
        return $this->routingService->renderJsonErrorResponse(404);
    }

    /**
     * @param UpdateNote $request
     * @param int $id
     * @return JsonResponse
     */
    public function update(UpdateNote $request, int $id): JsonResponse
    {
        if ($resource = $this->noteService->updateNote($id, $request->validated())) {
            return $this->routingService->renderJsonSuccessResponse(202, '', new NoteResource($resource));
        }
        return $this->routingService->renderJsonErrorResponse(204);
    }

    /**
     * @param int $id
     * @return JsonResponse
     */
    public function destroy(int $id): JsonResponse
    {
        $this->noteService->deleteNote($id);
        return $this->routingService->renderJsonSuccessResponse(204);
    }
}
