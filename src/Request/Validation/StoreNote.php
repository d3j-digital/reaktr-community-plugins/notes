<?php

namespace D3JDigital\Notes\Request\Validation;

use Illuminate\Foundation\Http\FormRequest;
use D3JDigital\Notes\Response\Entities\NoteEntity;
use Illuminate\Validation\Rule;

class StoreNote extends FormRequest
{
    /**
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            'title' => 'sometimes|string',
            'content' => 'required|string',
            'author_id' => 'sometimes|int',
            'author_first_name' => 'sometimes|string',
            'author_last_name' => 'sometimes|string',
            'visibility' => ['required', Rule::in(NoteEntity::getAvailableVisibilities())],
        ];
    }

    /**
     * @return array
     */
    public function messages(): array
    {
        return [
            'required' => 'this field is required',
            'visibility.in' => 'you can only specify one of the following accepted visibilities (' . implode(',', NoteEntity::getAvailableVisibilities()) . ')',
        ];
    }
}
