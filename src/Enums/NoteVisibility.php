<?php

namespace D3JDigital\Notes\Enums;

enum NoteVisibility
{
    case PUBLIC;
    case PRIVATE;
}
