<?php

namespace D3JDigital\Notes\Contracts\Services;

use Illuminate\Support\Collection;
use OpenSourceDeveloper\Reaktr\Core\Abstracts\Entity;
use OpenSourceDeveloper\Reaktr\Core\Response\LengthAwarePaginator;

interface NoteServiceInterface
{
    /**
     * @param array $relations
     * @param array|string[] $columns
     * @param bool $paginate
     * @return LengthAwarePaginator|Collection
     */
    public function getNotes(array $relations = [], array $columns = ['*'], bool $paginate = true): LengthAwarePaginator|Collection;

    /**
     * @param int $id
     * @param array $relations
     * @param array|string[] $columns
     * @return Entity|null
     */
    public function getNote(int $id, array $relations = [], array $columns = ['*']): ?Entity;

    /**
     * @param array $attributes
     * @return Entity|null
     */
    public function createNote(array $attributes): ?Entity;

    /**
     * @param int $id
     * @param array $attributes
     * @param bool $withoutLoadingModel
     * @return Entity|null
     */
    public function updateNote(int $id, array $attributes, bool $withoutLoadingModel = false): ?Entity;

    /**
     * @param int|array $ids
     * @param bool $forceDelete
     */
    public function deleteNote(int|array $ids, bool $forceDelete = false): void;
}
