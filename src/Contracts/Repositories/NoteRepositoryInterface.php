<?php

namespace D3JDigital\Notes\Contracts\Repositories;

use OpenSourceDeveloper\Reaktr\Core\Contracts\Repositories\Repository;

interface NoteRepositoryInterface extends Repository
{

}
