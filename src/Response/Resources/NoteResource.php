<?php

namespace D3JDigital\Notes\Response\Resources;

use OpenSourceDeveloper\Reaktr\Core\Response\Resources\JsonResource;
use D3JDigital\Notes\Filters\NoteFilter;
use D3JDigital\Notes\Response\Entities\NoteEntity;

/**
 * @mixin NoteEntity
 */
class NoteResource extends JsonResource
{
    public function toArray($request)
    {
        $data = [
            'id' => $this->id,
            'type' => 'note',
            'attributes' => [
                'id' => $this->id,
                'linked_resource_id' => $this->linkedResourceId,
                'linked_resource_type' => $this->linkedResourceType,
                'title' => $this->title,
                'content' => $this->content,
                'author_id' => $this->authorId,
                'author_first_name' => $this->authorFirstName,
                'author_last_name' => $this->authorLastName,
                'visibility' => $this->visibility,
                'created_at' => $this->createdAt,
                'updated_at' => $this->updatedAt,
                'deleted_at' => $this->deletedAt,
            ],
        ];

        return app()->triggerFilter(NoteFilter::PREPARE_NOTE_RESOURCE, $data, $this);
    }
}
