<?php

namespace D3JDigital\Notes\Response\Entities;

use OpenSourceDeveloper\Reaktr\Core\Abstracts\Entity;
use D3JDigital\Notes\Filters\NoteFilter;
use D3JDigital\Notes\Enums\NoteVisibility;

class NoteEntity extends Entity
{
    public ?int $id = null;
    public ?int $linkedResourceId = null;
    public ?string $linkedResourceType = null;
    public ?string $title = null;
    public ?string $content = null;
    public ?int $authorId = null;
    public ?string $authorFirstName = null;
    public ?string $authorLastName = null;
    public ?string $visibility = null;

    /**
     * @return array
     */
    public static function getAvailableVisibilities(): array
    {
        return app()->triggerFilter(NoteFilter::GET_NOTE_VISIBILITIES, collect(NoteVisibility::cases())->pluck('name')->toArray());
    }
}
