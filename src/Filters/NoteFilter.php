<?php

namespace D3JDigital\Notes\Filters;

class NoteFilter
{
    const GET_NOTE_VISIBILITIES = 'plugin.notes.filter.get-note-visibilities';
    const PREPARE_NOTE_RESOURCE = 'plugin.notes.filter.prepare-note-resource';
}
