<?php

namespace D3JDigital\Notes\Services;

use Illuminate\Support\Collection;
use OpenSourceDeveloper\Reaktr\Core\Abstracts\Entity;
use OpenSourceDeveloper\Reaktr\Core\Response\LengthAwarePaginator;
use OpenSourceDeveloper\Reaktr\Core\Response\PaginationResponse;
use D3JDigital\Notes\Contracts\Services\NoteServiceInterface;
use D3JDigital\Notes\Database\Eloquent\Repositories\NoteRepository;
use D3JDigital\Notes\Response\Entities\NoteEntity;

class NoteService implements NoteServiceInterface
{
    public function __construct(
        protected NoteRepository $noteRepository,
    ) {}

    public function getNotes(array $relations = [], array $columns = ['*'], bool $paginate = true): LengthAwarePaginator|Collection
    {
        $result = $this->noteRepository->all($relations, $columns, $paginate);
        if ($result instanceof PaginationResponse) {
            return new LengthAwarePaginator(collect($result->getRecords())->mapInto(NoteEntity::class), $result->getRecordCount(), $result->getRecordsPerPage(), $result->getCurrentPage());
        }
        return collect($result)->mapInto(NoteEntity::class);
    }

    public function getNote(int $id, array $relations = [], array $columns = ['*']): ?Entity
    {
        $result = $this->noteRepository->find($id, $relations, $columns);
        return $result ? (new NoteEntity)->fill($result) : null;
    }

    public function createNote(array $attributes): ?Entity
    {
        $result = $this->noteRepository->create($attributes);
        return $result ? (new NoteEntity)->fill($result) : null;
    }

    public function updateNote(int $id, array $attributes, bool $withoutLoadingModel = false): ?Entity
    {
        $result = $this->noteRepository->update($id, $attributes, $withoutLoadingModel);
        return $result ? (new NoteEntity)->fill($result) : null;
    }

    public function deleteNote(int|array $ids, bool $forceDelete = false): void
    {
        $this->noteRepository->delete($ids, $forceDelete);
    }
}
