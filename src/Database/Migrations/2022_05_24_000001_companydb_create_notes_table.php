<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CompanydbCreateNotesTable extends Migration
{
    public function up(): void
    {
        if (!Schema::hasTable('notes')) {
            Schema::create('notes', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->bigInteger('linked_resource_id')->nullable();
                $table->string('linked_resource_type')->nullable();
                $table->string('title')->nullable();
                $table->longText('content')->nullable();
                $table->unsignedBigInteger('author_id')->nullable();
                $table->string('author_first_name')->nullable();
                $table->string('author_last_name')->nullable();
                $table->string('visibility');
                $table->timestamps();
                $table->softDeletes();
                $table->foreign('author_id')->references('id')->on('core_users')->onDelete('cascade');
                $table->index(['linked_resource_id', 'linked_resource_type'], 'noteable');
            });
        }
    }

    public function down(): void
    {
        Schema::dropIfExists('notes');
    }
}
