<?php

use Illuminate\Database\Migrations\Migration;
use OpenSourceDeveloper\Reaktr\Core\Contracts\Services\AuthService;
use OpenSourceDeveloper\Reaktr\Core\Base\PermissionObject;

class CompanydbAddNotePermissions extends Migration
{
    protected AuthService $authService;

    public function __construct()
    {
        $this->authService = app(AuthService::class);
    }

    public function up(): void
    {
        $permissions = [
            new PermissionObject('notes.*', 'reaktr.permissions.global.all_actions', ['resource' => 'plugin-notes.resources.note.name.singular']),
            new PermissionObject('notes.index', 'reaktr.permissions.global.list', ['resource' => 'plugin-notes.resources.note.name.singular']),
            new PermissionObject('notes.store', 'reaktr.permissions.global.create', ['resource' => 'plugin-notes.resources.note.name.singular']),
            new PermissionObject('notes.show', 'reaktr.permissions.global.view', ['resource' => 'plugin-notes.resources.note.name.singular']),
            new PermissionObject('notes.update', 'reaktr.permissions.global.update', ['resource' => 'plugin-notes.resources.note.name.singular']),
            new PermissionObject('notes.destroy', 'reaktr.permissions.global.delete', ['resource' => 'plugin-notes.resources.note.name.singular']),
        ];

        foreach ($permissions as $permission) {
            $this->authService->createPermission($permission);
        }
    }
}
