<?php

namespace D3JDigital\Notes\Database\Eloquent\Models;

use Illuminate\Database\Eloquent\Relations\MorphTo;
use Illuminate\Database\Eloquent\SoftDeletes;
use OpenSourceDeveloper\Reaktr\Core\Abstracts\Eloquent\Model;

class Note extends Model
{
    use SoftDeletes;

    /**
     * @var string
     */
    protected $table = 'notes';

    /**
     * @var array
     */
    protected $guarded = [];

    /**
     * @return MorphTo
     */
    public function noteable(): MorphTo
    {
        return $this->morphTo();
    }
}
