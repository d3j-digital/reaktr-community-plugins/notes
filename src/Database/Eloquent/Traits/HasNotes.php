<?php

namespace D3JDigital\Notes\Database\Eloquent\Traits;

use Illuminate\Database\Eloquent\Relations\MorphMany;
use D3JDigital\Notes\Database\Eloquent\Models\Note;

class HasNotes
{
    /**
     * @return MorphMany
     */
    public function notes(): MorphMany
    {
        return $this->morphMany(Note::class, 'linked_resource');
    }
}
