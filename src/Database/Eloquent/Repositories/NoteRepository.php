<?php

namespace D3JDigital\Notes\Database\Eloquent\Repositories;

use OpenSourceDeveloper\Reaktr\Core\Abstracts\Eloquent\Model;
use OpenSourceDeveloper\Reaktr\Core\Abstracts\Eloquent\Repository;
use OpenSourceDeveloper\Reaktr\Core\Abstracts\Eloquent\SystemModel;
use D3JDigital\Notes\Contracts\Repositories\NoteRepositoryInterface;
use D3JDigital\Notes\Database\Eloquent\Models\Note;

class NoteRepository extends Repository implements NoteRepositoryInterface
{
    /**
     * @return Model|SystemModel
     */
    function model(): Model|SystemModel
    {
        return new Note();
    }
}
